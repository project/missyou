<?php

/**
 * @file
 * Requirements page for I Miss You.
 */

/**
 * Implements hook_requirements().
 */
function missyou_requirements($phase) {
  $requirements = [];

  // Verify Hover is enabled.
  if ($phase == 'runtime') {
    /** @var Drupal\Core\Asset\LibraryDiscovery $library_discovery */
    $library_discovery = \Drupal::service('library.discovery');
    $library           = $library_discovery->getLibraryByName('missyou', 'missyou');
    $has_missyou       = _missyou_verify_library($library);

    $requirements['missyou'] = [
      'title'    => t('I Miss You library'),
      'severity' => $has_missyou ? REQUIREMENT_OK : REQUIREMENT_WARNING,
      'value'    => $has_missyou ? t('Enabled') : t('Not found'),
    ];
    if (!$has_missyou) {
      $download_url                           = 'https://github.com/Bahlaouane-Hamza/I-Miss-You/archive/master.zip';
      $requirements['missyou']['description'] = t('I Miss You module requires the I Miss You library. <a href=":anim_link">Download I Miss You library</a> and unzip into /libraries/missyou.', [':anim_link' => $download_url]);
    }
  }

  return $requirements;
}

/**
 * Verify that the library files exist.
 *
 * @param array $library
 *   The library to verify.
 *
 * @return bool
 *   TRUE if all files of this library exists, FALSE otherwise
 *
 * @see https://drupal.org/node/2231385
 */
function _missyou_verify_library(array $library) {

  $exists = TRUE;
  if (array_key_exists('js', $library)) {
    foreach ($library['js'] as $css) {
      if ($css['type'] == 'file') {
        if (!file_exists(DRUPAL_ROOT . '/' . $css['data'])) {
          $exists = FALSE;
        }
      }
    }
  }

  return $exists;
}
