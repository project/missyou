---DESCRIPTION---

Simple module to change the title & favicon of the page when the user is not
viewing your website.

---Dependencies---

Libraries
I Miss You in libraries/missyou
(can be downloaded from https://github.com/Bahlaouane-Hamza/I-Miss-You).

---Module usage---

Once you have enabled this module and add the library, then you can configure
your title and favicon on '/admin/config/missyou/config' page.

---INSTALLATION AND CONFIGURATION---

1. Download and ADD the library from
https://github.com/Bahlaouane-Hamza/I-Miss-You in libraries/missyou

2. Install the module as usual.

3. Go to '/admin/config/missyou/config' and set your title and favicon
